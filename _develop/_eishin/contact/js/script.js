function just_number(input) {
 input.addEventListener('keypress',function(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      evt.preventDefault();
    return true;
  })
}
function limit(element,max) {
  var max_chars = max;
  if(element.value.length > max_chars) {
    element.value = element.value.substr(0, max_chars);
  }
}

window.addEventListener('DOMContentLoaded',function(){
  var _3digit = document.getElementById('3digits');
  var _4digit = document.getElementById('4digits');
  var _postal = document.getElementById('postal');
  var _pc = document.getElementById('postal_code');
  just_number(_3digit);
  just_number(_4digit);
  _3digit.addEventListener('keydown',function(){
    limit(this,3);
  })
  _3digit.addEventListener('keyup',function(){
    limit(this,3);
  })
  _4digit.addEventListener('keydown',function(){
    limit(this,4);
  })
  _4digit.addEventListener('keyup',function(){
    limit(this,4);
  })
  _postal.addEventListener('click',function(e){
    e.preventDefault();
    var postal_code = _3digit.value+'-'+_4digit.value;
    _pc.value = postal_code;
    _pc.dispatchEvent(new Event('keyup'));
  })
});
