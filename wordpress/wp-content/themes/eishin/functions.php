<?php
require_once("meta-box-class/my-meta-box-class.php");
require_once("Tax-meta-class/tax-meta-class.php");
add_action('admin_enqueue_scripts', 'wdscript');
function wdscript() {
  wp_register_style('custom_wp_admin_css', get_template_directory_uri() . '/admin-style.css', false, '1.0.0');
  wp_enqueue_style('custom_wp_admin_css');
}
function enqueue_script(){
  wp_enqueue_script('libs',get_template_directory_uri().'/common/js/libs.js', '1.0', 1 );
  if(is_front_page()) {
    wp_enqueue_script('toppage', get_template_directory_uri().'/js/script.js', '1.0', 1 );
  }
  if(is_page_template('contact/contact.php')){
    wp_enqueue_script('contact_libs','https://yubinbango.github.io/yubinbango/yubinbango.js', '1.0', 1 );
    wp_enqueue_script('contact_script', get_template_directory_uri().'/contact/js/script.js', '1.0', 1 );
  }
  if(is_single()){
    wp_enqueue_script('bootstrap-datepicker', get_template_directory_uri().'/news/js/bootstrap-datepicker.js', '1.0', 1 );
    wp_enqueue_script('bootstrap-lang', get_template_directory_uri().'/news/js/bootstrap-datepicker.ja.js', '1.0', 1 );
    wp_enqueue_script('news_script', get_template_directory_uri().'/news/js/scripts.js', '1.0', 1 );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_script');
function enqueue_style() {
  if(is_front_page()) {
    wp_enqueue_style('toppage',get_stylesheet_directory_uri().'/css/style.css');
  }
  if(is_page_template('case/case.php') || is_singular('case') || is_tax('tag_case')) {
    wp_enqueue_style('case-style',get_stylesheet_directory_uri().'/case/css/style.css');
  }
  if(is_page_template('contact/contact.php')) {
    wp_enqueue_style('contact-style',get_stylesheet_directory_uri().'/contact/css/style.css');
  }
  if(is_page_template('news/news.php') || is_single() || is_category()) {
    wp_enqueue_style('news-style',get_stylesheet_directory_uri().'/news/css/style.css');
    wp_enqueue_style('bootstrap-datepicker3',get_stylesheet_directory_uri().'/news/css/bootstrap-datepicker3.css');
  }
  if(is_page_template('about/about.php')) {
    wp_enqueue_style('about-style',get_stylesheet_directory_uri().'/about/css/style.css');
  }
  if(is_page_template('concept/concept.php')) {
    wp_enqueue_style('concept-style',get_stylesheet_directory_uri().'/concept/css/style.css');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_style' );
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');
add_filter('wpcf7_form_elements', function($content) {
  $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
  return $content;
});
add_filter('wpcf7_form_elements', function($content) {
  $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
  return $content;
});
// post type case //
function case_arr(){
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'List Case',
      'singular_name' => 'case',
      'add_new_item' => 'New Case',
    ),
    'supports' => array(
      'title','editor'
    )
  );
  register_post_type('case',$args);
}
add_action('init','case_arr');
$config_case = array(
  'id' => 'case',
  'title' => 'Case Detail',
  'pages' => array('case'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$case_meta = new AT_Meta_Box($config_case);
$case_meta->addText('case_location',array('name'=> 'Case Location','style'=>'max-width :100%; width:100%; height:auto'));
$case_meta->addImage('case_image',array('name'=> 'Case Thumbnail','style'=>'max-width :100%; width:100%; height:auto'));
$info[] = $case_meta->addText('case_info_title',array('name'=>'Case Info Title'),true);
$info[] = $case_meta->addText('case_info_content',array('name'=>'Case Info Content'),true);
$case_meta->addRepeaterBlock('case_info',array('inline'=>true,'name'=>'Case Info','fields'=> $info));
$compare[] = $case_meta->addImage('compare_image_before',array('name'=>'Image Before'),true);
$compare[] = $case_meta->addImage('compare_image_after',array('name'=>'Image After'),true);
$compare[] = $case_meta->addTextarea('commpare_result',array('name'=>'Compare Result'),true);
$case_meta->addRepeaterBlock('case_compare',array('name'=>'Case Compare','fields'=> $compare));
$point[] = $case_meta->addImage('point_image',array('name'=>'Point Image'),true);
$point[] = $case_meta->addTextarea('point_text',array('name','Point Text'),true);
$case_meta->addRepeaterBlock('case_point',array('name'=>'Case Point','fields'=>$point));
$gallery[] = $case_meta->addImage('gallery_image',array('name'=>'Image Case'),true);
$case_meta->addRepeaterBlock('case_gallery',array('name' => 'Case Gallery','fields'=>$gallery));
$case_meta->Finish();
function create_tag_case(){
  $labels = array(
    'name'              => _x( 'Name Tag Case', 'taxonomy general tag name' ),
    'singular_name'     => _x( 'Tag Case', 'taxonomy singular tag name' ),
    'search_items'      => __( 'SearchTag  Case' ),
    'all_items'         => __( 'All Tag Case' ),
    'parent_item'       => null,
    'parent_item_colon' => null,
    'edit_item'         => __( 'Edit Tag Case' ),
    'update_item'       => __( 'Update Tag Case' ),
    'add_new_item'      => __( 'Add Tag New Case' ),
    'new_item_name'     => __( 'New Tag Case' ),
    'menu_name'         => __( 'Tag Case' ),
  );
  $args = array(
    'hierarchical' => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'tag_case' ),
  );
  register_taxonomy( 'tag_case', array( 'case' ), $args );
}
add_action( 'init', 'create_tag_case', 0 );
// end case
// news
add_theme_support('post-thumbnails');
set_post_thumbnail_size( 202,196,true);
// end news
function action_taxo($taxo){
  $terms = get_terms(array(
    'taxonomy' => $taxo,
    'hide_empty' => false,
    'orderby' => 'ID',
    'order' => 'asc',
    'parent' => 0
  ));
  return $terms;
}
add_filter('list_taxo','action_taxo');
function action_case($name='') {
  $search_tax = array();
  if($name) {
    $tax = array(
      'taxonomy' => 'tag_case',
      'field' => 'slug',
      'terms' => $name
    );
    array_push($search_tax,$tax);
  }
  $args = array(
    'post_type' => 'case',
    'orderby'   => 'id',
    'order'     => 'desc',
    'tax_query' => $search_tax,
    'posts_per_page' => -1
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('list_case','action_case',10,1);
function action_3_case(){
  global $post;
  $args = array(
    'post_type' => 'case',
    'orderby'   => 'id',
    'order'     => 'desc',
    'post__not_in' => array($post->ID),
    'posts_per_page' => 3
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_3_case','action_3_case',10,1);

function action_3_random_case($term){
  global $post;
  $args = array(
    'post_type' => 'case',
    'orderby'   => 'id',
    'order'     => 'desc',
    'post__not_in' => array($post->ID),
    'tax_query' => array(
      array(
        'taxonomy' => 'tag_case',
        'field' => 'id',
        'terms' => $term,
      )
    ),
    'posts_per_page' => 3
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_3_random_case','action_3_random_case',10,2);


function mp_pagination($prev = 'Prev', $next = 'Next', $pages='') {
    global $wp_query, $wp_rewrite, $textdomain;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    if(array_key_exists('pg',$_GET)){
      if($_GET['pg']) {
        $pg = $_GET['pg'];
      } else {
        $pg = 1;
      }
    } else {
      $pg = 1;
    }
    $big = 999999999;
    $pagination = array(
    'base'      => @add_query_arg( 'pg', '%#%' ),
    'format'   => '',
    'current'   => max ( 1, $pg),
    'total'   => $pages,
    'prev_text' => __($prev,$textdomain),
    'next_text' => __($next,$textdomain),
    'type'   => 'list',
    'end_size'  => 1,
    'mid_size'  => 2
    );
    $return =  paginate_links( $pagination );
    echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return );
}
function action_articles($name = '',$get) {
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $year = '';
  $month = '';
  $day = '';
  if(array_key_exists('date',$get)) {
    if($get['date']) {
      $date = trim($get['date'],' ');
      $date = explode('/',$date);
      $year = $date[0];
      $month = $date[1];
      $day = $date[2];
    }
  }
  $search_tax = array();
  if($name) {
    $tax = array(
      'taxonomy' => 'category',
      'field' => 'slug',
      'terms' => $name
    );
    array_push($search_tax,$tax);
  }
  $args  = array(
    'post_type' => 'post',
    'orderby'   => 'id',
    'order'     => 'desc',
    'year' => $year,
    'month' => $month,
    'day'  => $day,
    'paged' => $paged,
    'tax_query' => $search_tax,
    'posts_per_page' => 4,
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('list_article','action_articles',10,2);

function action_4_news(){
  global $post;
  $args = array(
    'post_type' => 'post',
    'orderby'   => 'id',
    'order'     => 'desc',
    'posts_per_page' => 4
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_4_news','action_4_news',10,1);
?>