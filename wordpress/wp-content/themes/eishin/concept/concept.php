<?php
/*
Template Name: Concept Page
*/
get_header();
?>
<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <div class="gr_ttl_left">
          <h2 class="ttl">Concept<span>コンセプト</span></h2>
        </div>
        <!--/.left-->
        <div class="gr_ttl_right">
          <figure>
            <img src="<?php bloginfo('template_url')?>/concept/images/img_ttl.jpg?v=d8d591e4b1b1033a1d126f891dce9634" alt="Images title">
          </figure>
        </div>
        <!--/.right-->
      </div>
    </div>
    <div class="gr_breadcrumb show_pc">
      <div class="row">
        <ul class="gr_breadcrumb_list">
          <li><a href="<?php _e(home_url())?>">ホーム</a></li>
          <li>コンセプト</li>
        </ul>
        <!--/.list-->
      </div>
    </div>
    <div class="think">
      <figure>
        <img src="<?php bloginfo('template_url')?>/concept/images/think.jpg" alt="">
        <figcaption>
          <em>4<small>つの思い</small></em>
          <span>家族をつつむ つよく やさしい 自然の庭</span>
        </figcaption>
      </figure>
    </div>
    <div class="polygon">
      <div class="polygon_row row">
        <ul>
          <li><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_01.png?v=85790b579a65e23f7f1aaa5b7503192c" alt="polygon 01"></li>
          <li><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_02.png?v=c236c054da72910f01adc1084c7ced56" alt="polygon 02"></li>
          <li><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_03.png?v=6e0059bfef72d11741b5b3a506adde7c" alt="polygon 03"></li>
          <li><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_04.png?v=1e5a0d21ee772c9cc24ac1b3b25b5a0b" alt="polygon 04"></li>
          <li><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_05.png?v=781789d10af6f1f64fbaebb31259facf" alt="polygon 05"></li>
        </ul>
        <p>家に帰るとポワッとあたたかく門を照らす光<br>「おかえり♡」とやさしくゆれる木々。<br>住人がいない間も家を守る番人のような頼もしいエクステリア。</p>
      </div>
    </div>
    <div class="make">
      <h3 class="ttl_gr"><span>庭づくりは「笑顔づくり」</span></h3>
      <p>ここで言う庭とは家の一歩外から敷地内全てのエリアのこと。<br>家の中から見る景色もさることながら、アウトドアエリアにも目<br>を向けて視野が広がり、楽しみが広がることで「あぁ～幸せ♡」<br>の瞬間が少しでも増えること。笑顔の時が続くこと。<br>それこそがわたしたちの一番の願いです。</p>
      <div class="make_wrap row">
        <article>
          <figure><img src="<?php bloginfo('template_url')?>/concept/images/make_01.jpg" alt="make 01"><span><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_01.png" alt=""></span></figure>
          <div class="make_right">
            <h3 class="color1">家族をつつむ？</h3>
            <p>全国のエクステリア・外構工事にかかる費用の相場は、住宅建設費の１割が目安と言われています。<br>せっかくお金をかけて作るのなら今までより便利に、自分たちの生活にピッタリと、合わせたものがいいと思います。<br>何が必要で、何ができるのか…ヒアリングと話し合いを重ねる事が大切です。<br>予算に合わせてラクでおしゃれ、手がかからない。そんなデザインを心がけます。</p>
          </div>
        </article>
        <article>
          <figure><img src="<?php bloginfo('template_url')?>/concept/images/make_02.jpg" alt="make 02"><span><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_02.png" alt=""></span></figure>
          <div class="make_right">
            <h3 class="color2">つよい構造のエクステリア</h3>
            <p>大切なことは、見えない所がしっかりと頑丈に作られているか。<br>デザインが良くても、基礎部分の材料が安価なものに替えられたり、必要なものが入っていなかったり、そんな手抜き工事が横行しているそうです。<br>大幅に値引き、格安を謳う業者はおすすめできません。<br>地盤の状態を確かめ、鉄筋の太さや、コンクリートの厚み（呼び強度）季節や気温、天候も考慮し正しい判断をしなければなりません。仕事に対するまじめさと、たくさんの経験が必要です。<br>わたしたちにとって、一邸一邸がたからもの。<br>「イイモノ」を作りたいという職人としての思いと、情熱。<br>お客様の気持ちと職人技を合わせる事で「イイモノ」が出来上がります。</p>
          </div>
        </article>
        <article>
          <figure><img src="<?php bloginfo('template_url')?>/concept/images/make_03.jpg" alt="make 03"><span><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_03.png" alt=""><img src="<?php bloginfo('template_url')?>/concept/images/icon_polygon_04.png" alt=""></span></figure>
          <div class="make_right">
            <h3>パッシブガーデン・<span class="color3">心にやさしい</span><span class="color4">自然の庭</span></h3>
            <p>日の入り方、風向きを考えて、樹木を植え、木陰をつくることで夏涼しく冬あたたかい自然の力を借りたエコな庭。<br>キラキラ光る木漏れ日を浴びてテラスで朝ごはん、綺麗な芝生に寝転んで木々をすりぬける風が心地いい庭…<etcbr></etcbr>緑のパワーで何倍も楽しく、心が潤い満たされます。<br>雑木が涼し気な軽井沢のような庭はいかがですか？</p>
          </div>
        </article>
      </div>
      <div class="make_result row">
        <article>
          <div class="make_result_t">
            <div class="make_result_ttl">
              <span>Exterior renovation</span>
              <h3>エクステリアリフォーム</h3>
            </div>
            <p>長い間生活を共にしたエクステリア。<br>構造的な劣化の不安や、生活変化にともなうお悩みをヒアリング。<br>住みごこちの良いデザインを提案いたします。</p>
          </div>
          <dl>
            <dt>ブロック </dt>
            <dd>解体や増設、既存ブロックに左官のぬりかべでリフォームも可能</dd>
          </dl>
          <dl>
            <dt>門　柱</dt>
            <dd>壊れたポスト、古くなった門柱などはおしゃれ<br>な機能門柱に。宅配BOXもオススメです。</dd>
          </dl>
          <dl>
            <dt>車　庫</dt>
            <dd>車の台数に応じて土間コンクリートの増設、<br>カーポートガレージのご提案</dd>
          </dl>
          <figure><img src="<?php bloginfo('template_url')?>/concept/images/img_05.png" alt=""></figure>
        </article>
        <article>
          <div class="make_result_t">
            <div class="make_result_ttl">
              <span>Renovation</span>
              <h3><small>暮らすガーデン</small>「リノベーション」</h3>
            </div>
            <p>毎日を楽しく、もっと便利に過ごしたい。<br>ご家族が笑顔になれるお庭にグレードアップ。<br>生活動線からベストなリノベーションをご提案。</p>
          </div>
          <dl>
            <dt>カーポート</dt>
            <dd>スタイリッシュからナチュラルまでたくさんの<br>種類からお選びいただけます。</dd>
          </dl>
          <dl>
            <dt>シェード</dt>
            <dd>日差しをコントロールして、冷房費1/3に。夏の暑さ対策にオススメです。</dd>
          </dl>
          <dl>
            <dt>ウッドデッキ</dt>
            <dd>住宅の形状、便利な動線を考えてどんな形にも加工できます。</dd>
          </dl>
          <dl>
            <dt>水　栓</dt>
            <dd>場所の移動や増設など。収納付きシンクタイプもオススメ。</dd>
          </dl>
          <figure><img src="<?php bloginfo('template_url')?>/concept/images/img_04.png" alt=""></figure>
        </article>
        <article>
          <div class="make_result_t">
            <div class="make_result_ttl">
              <span>Barrierfreedesign</span>
              <h3>バリアフリーデザイン</h3>
            </div>
            <p>小さなお子様やお年寄りにも安心安全に<br>暮らせるデザインをご提案。<br>南欧風やリゾートモダンなど今までにない<br>意匠性のあるバリアフリー設計。</p>
          </div>
          <figure><img src="<?php bloginfo('template_url')?>/concept/images/img_03.png" alt=""></figure>
        </article>
        <article>
          <div class="make_result_t">
            <div class="make_result_ttl">
              <span>Green & natural</span>
              <h3>グリーン＆ナチュラル</h3>
            </div>
            <p>木や花々、天然石をたくさん使い、いつも<br>緑がそばにあるお庭に。<br>家庭菜園や花壇、ビオトープもオリジナル<br>デザインいたします。住宅に合わせた樹木の<br>選別から植栽も行います。手入れの方法や<br>アフターフォローでサポートします。</p>
          </div>
          <figure><img src="<?php bloginfo('template_url')?>/concept/images/img_02.png" alt=""></figure>
        </article>
      </div>
      <div class="make_btn">
        <a href="<?php _e(home_url())?>/case">施工実績</a><a href="<?php _e(home_url())?>/news">新着情報</a>
      </div>
    </div>
    <div class="flow" id="flow">
      <h3 class="ttl_gr"><span>施工までの流れ</span></h3>
      <p>栄進エクステリアの庭づくりの流れをご紹介します。<span>webのお問い合わせフォーム、又はお電話、LINEにてご連絡ください</span></p>
      <div class="flow_wrap">
        <div class="flow_wrap_row row">
          <article>
            <figure><img src="<?php bloginfo('template_url')?>/concept/images/flow_01.jpg" alt=""><span>01</span></figure>
            <div class="right">
              <h3>訪問・来店相談</h3>
              <p>敷地の状態を確認できる図面一式（平面・立面図）をご用意ください。デザイナーがご自宅に伺い、お庭の現状確認・ご要望をヒアリングいたします。<br>又、新築中（予定）の方は、図面一式をご持参の上ご来店ください。</p>
              <dl>
                <dt>POINT</dt>
                <dd>ネットで見つけたイメージ写真、雑誌の切り抜きがあるとお気に入りに近付きます!!イメージがわからない！という方も、カタログを見ながらステキなデザインを見つけましょう。</dd>
              </dl>
            </div>
          </article>
          <article>
            <figure><img src="<?php bloginfo('template_url')?>/concept/images/flow_02.jpg" alt=""><span>02</span></figure>
            <div class="right">
              <h3>ファーストプラン・お打ち合わせ</h3>
              <p>ご来店いただき、3DCADパースを実際に動かしながら、打ち合わせをいたします。お庭の課題や、ご要望ご予算、ライフスタイルに合わせてプランを作成しご提案いたします。<br>お見積りの詳細につきましてもじっくりお話させていただきます。</p>
              <div class="right_wrap">
                <dl>
                  <dt>アドバイス１</dt>
                  <dd>一生に一度!？の高いお買い物。あれがいい…これがいい…わがままをたくさん話してこだわりをつめこもう！コーヒーを飲みながらゆっくりお話しください。</dd>
                </dl>
                <dl>
                  <dt>アドバイス２</dt>
                  <dd>エクステリア工事を１度に全ての範囲で行うとなると結構な出費です。初めから工事を2期3期に分けて作っていくプランもオススメ。</dd>
                </dl>
              </div>
            </div>
          </article>
          <article>
            <figure><img src="<?php bloginfo('template_url')?>/concept/images/flow_03.jpg" alt=""><span>03</span></figure>
            <div class="right">
              <h3>プラン決定・ご契約</h3>
              <p>お見積りプランをご検討いただき、納得いただけるプランが決定しましたら、晴れてご契約となります。<br>ご契約時に手付金、着工前に着工金、工事完了後に完工金をお支払いいただきます。エクステリアローンの相談も承ります。</p>
            </div>
          </article>
          <article>
            <figure><img src="<?php bloginfo('template_url')?>/concept/images/flow_04.jpg" alt=""><span>04</span></figure>
            <div class="right">
              <h3>施工開始・お引渡し</h3>
              <p>ご契約後、資材等の手配を行い、着工スケジュールを調整し施工開始となります。<br>工事完了後、お客様と最終確認を行いお引渡しとなります。</p>
            </div>
          </article>
          <article>
            <figure><img src="<?php bloginfo('template_url')?>/concept/images/flow_05.jpg" alt=""><span>05</span></figure>
            <div class="right">
              <h3>アフターフォロー・メンテナンス</h3>
              <p>当社の独自の工事保証基準を定めております。<br>又、有料にて植木の年間管理も行っております。（芝刈・剪定等）</p>
              <dl>
                <dt>アピール！</dt>
                <dd>理想のお庭が完成!!ここから新たな暮らしのスタートです！<br>何かささいな事でもお困りの事がありましたらすぐにお知らせください。どうぞ末永いお付き合いを。</dd>
              </dl>
            </div>
          </article>
        </div>
      </div>
    </div>
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>