<?php
  get_header();
?>
<body>
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div id="container" class="container">
      <header id="header" class="header">
        <?php get_template_part('template/template','header')?>
      </header>
      <main>
        <div class="gr_ttl">
          <div class="row wrap">
            <div class="gr_ttl_left">
              <h2 class="ttl">Case<span>施工実績</span></h2>
            </div>
            <!--/.left-->
            <div class="gr_ttl_right">
              <figure>
                <img src="<?php bloginfo('template_url')?>/case/images/img_ttl.jpg" alt="Images title">
              </figure>
            </div>
            <!--/.right-->
          </div>
        </div>
        <!--/.gr_ttl-->
        <div class="gr_breadcrumb">
          <div class="row">
            <ul class="gr_breadcrumb_list">
              <li><a href="/">ホーム</a></li>
              <li><a href="/case">施工実績</a></li>
              <li><?php the_title()?></li>
            </ul>
            <!--/.list-->
          </div>
        </div>
        <!--/.gr_breadcrumb-->
        <div class="gr_case">
          <h3 class="ttl_gr"><span><?php the_title()?></span></h3>
          <div class="gr_case_detail b_bkg">
            <div class="row wrap">
              <?php
                if(get_post_meta($post->ID,'case_image',true)) :
              ?>
              <figure>
                <img src="<?php _e(get_post_meta($post->ID,'case_image',true)['url'])?>" alt="images detail">
              </figure>
            <?php endif;?>
              <div class="txt_dt">
                <p><?php the_content();?></p>
                <?php
                  $infos = get_post_meta($post->ID,'case_info',true);
                  if($infos):
                ?>
                <ul class="price">
                  <?php foreach($infos as $info):?>
                  <li><?php _e($info['case_info_title'])?><span><?php _e($info['case_info_content'])?></span></li>
                  <?php  endforeach;?>
                </ul>
              <?php endif;?>
                <!--/.price-->
              </div>
              <!--/.txt_dt-->
              <?php
                $compares = get_post_meta($post->ID,'case_compare',true);
                if($compares):
              ?>
              <?php foreach($compares as $compare):?>
              <div class="dt_center">
                <div class="bf_af">
                  <?php if($compare['compare_image_before']):?>
                  <article>
                    <h4 class="ttl_h">Before</h4>
                    <figure>
                      <img src="<?php _e($compare['compare_image_before']['url'])?>" alt="sample">
                    </figure>
                  </article>
                  <?php endif;?>
                  <?php if($compare['compare_image_after']):?>
                  <article>
                    <h4 class="ttl_h">After</h4>
                    <figure>
                      <img src="<?php _e($compare['compare_image_after']['url'])?>" alt="sample">
                    </figure>
                  </article>
                  <?php endif;?>
                </div>
                <!--/.bf_af-->
                <div class="txt_dt">
                  <p><?php _e(nl2br($compare['commpare_result']))?></p>
                </div>
                <!--/.txt_dt-->
              </div>
              <?php endforeach;endif;?>
              <!--/.dt_center-->

              <?php
                $points = get_post_meta($post->ID,'case_point',true);
                if($points):
              ?>
              <h4 class="ttl_h">ここがポイント！</h4>
              <?php foreach($points as $point):?>
              <?php
                if($point['point_image']):
              ?>
              <figure>
                <img src="<?php _e($point['point_image']['url'])?>" alt="sample">
              </figure>
            <?php endif;?>
              <div class="txt_dt">
                <p><?php _e($point['point_text'])?></p>
              </div>
            <?php endforeach;endif;?>
              <!--/.txt_dt-->
              <div class="dt_bottom">
                <?php
                  $gallery = get_post_meta($post->ID,'case_gallery',true);
                  if($gallery):
                ?>
                <h4 class="ttl_h">施工の様子</h4>
                <ul class="list_img">
                  <?php foreach($gallery as $img):?>
                    <?php if($img['gallery_image']):?>
                  <li><img src="<?php _e($img['gallery_image']['url'])?>" alt="sample"></li>
                <?php endif;?>
                <?php endforeach;?>
                </ul>
              <?php endif;?>
              </div>
              <!--/.dt_bottom-->
              <div class="dt_post case_item">
                <h4 class="ttl_post">この実績を見ている人は、他にもこんな施工実績を見ています</h4>
                <div class="w_item">
                  <?php
                    $cate = wp_get_object_terms($post->ID, 'tag_case', array('fields'=>'ids'));
                    $cases = apply_filters('get_3_random_case',$cate);
                    while ($cases->have_posts()) : $cases->the_post();
                  ?>
                  <article>
                    <a href="<?php the_permalink()?>">
                      <figure>
                        <?php
                          $thumb = get_bloginfo('template_url').'/common/images/logo_sp.png';
                          if(get_post_meta($post->ID,'case_image',true)){
                            $thumb = get_post_meta($post->ID,'case_image',true)['url'];
                          }
                        ?>
                        <img src="<?php _e($thumb)?>" alt="Images Case 1">
                      </figure>
                      <div class="main_art">
                        <h4><?php the_title()?><span><?php _e(get_post_meta($post->ID,'case_location',true))?></span></h4>
                      </div>
                      <!--/.main_art-->
                    </a>
                  </article>
                  <?php endwhile;wp_reset_query();?>
                </div>
              </div>
              <!--/.dt_post-->
            </div>
          </div>
          <!--/.detail-->
          <div class="b_pag">
            <div class="row">
              <ul class="list_pag">
                <li class="prev">
                  <?php previous_post_link('%link','<span>前へ</span>');?>
                </li>
                <li><a href="/case">一覧へ戻る</a></li>
                <li class="next">
                  <?php next_post_link('%link','<span>次へ</span>'); ?>
                </li>
              </ul>
              <!--/.list_pag-->
            </div>
          </div>
          <!--/.b_pag-->
          <div class="c_tag">
            <h3 class="ttl_gr"><span>施工内容から探す</span></h3>
            <div class="case_tag">
              <div class="row">
                <ul class="list_tag">
                  <?php
                  $types = apply_filters('list_taxo','tag_case');
                  if($types):
                  foreach($types as $type) :
                  ?>
                  <li><a href="<?php _e(get_term_link($type))?>"><?php _e(nl2br($type->name)) ?></a></li>
                  <?php endforeach;endif;?>
                </ul>
                <!--/.list_tag-->
              </div>
          </div>
          <!--/.c_tag-->
        </div>
        <!--/.gr_case-->
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('template/template','footer')?>
      </footer>
    </div>
    <?php get_footer();?>
    <?php endwhile; endif; ?>
  </body>
  </html>