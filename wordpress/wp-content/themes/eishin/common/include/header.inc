<div class="header_main">
  <h1>
    <a href="/">
      <picture>
        <source media="(max-width:768px)" srcset="/common/images/logo_sp.png">
        <img src="/common/images/logo.png" alt="logo">
      </picture>
    </a>
  </h1>
  <div class="wrap_nav">
    <nav class="header_nav row" id="nav">
      <ul class="header_nav_list">
        <li><a href="/">HOME</a></li>
        <li><a href="/concept">コンセプト</a></li>
        <li class="has_child">
          <a href="/case">施工実績</a>
          <span class="sub"></span>
          <ul>
            <li><a href="">玄関アプローチ</a></li>
            <li><a href="">フェンス</a></li>
            <li><a href="">花壇</a></li>
          </ul>
        </li>
        <li class="has_child">
          <a href="/news">新着情報</a>
          <span class="sub"></span>
          <ul>
            <li><a href="">ブログ</a></li>
            <li><a href="">お知らせ</a></li>
          </ul>
        </li>
        <li class="has_child">
          <a href="/about">会社案内</a>
          <span class="sub"></span>
          <ul>
            <li><a href="/about#message">代表あいさつ</a></li>
            <li><a href="/about#company">会社概要</a></li>
            <li><a href="/about#access">アクセス</a></li>
          </ul>
        </li>
        <li class="show_sp"><a href="/contact">お問い合わせ</a></li>
      </ul>
      <ul class="header_nav_btn">
        <li><a href="/contact" class="mail">お問い合わせ</a></li>
        <li><a href="tel:0480-48-6177" class="tel">0480-48-6177</a></li>
      </ul>
    </nav>
  </div>
  <div class="menu_sp show_sp" id="icon_nav">
      <div class="icon_menu">
        <div class="icon_inner"></div>
      </div>
    </div>
</div>