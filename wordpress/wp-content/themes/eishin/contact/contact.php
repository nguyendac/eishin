<?php
/*
Template Name: Contact Page
*/
get_header();
?>
<body>
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <div class="gr_ttl_left">
          <h2 class="ttl">Contact<span>新着情報</span></h2>
        </div>
        <!--/.left-->
        <div class="gr_ttl_right">
          <figure>
            <img src="<?php bloginfo('template_url')?>/contact/images/img_ttl.jpg?v=18f07d4d756bf267a76bd0f1042ae94e" alt="Images title">
          </figure>
        </div>
        <!--/.right-->
      </div>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_breadcrumb">
      <div class="row">
        <ul class="gr_breadcrumb_list">
          <li><a href="/">ホーム</a></li>
          <li>無料相談・お見積もり</li>
        </ul>
        <!--/.list-->
      </div>
    </div>
    <!--/.gr_breadcrumb-->
    <div class="gr_contact">
      <div class="gr_contact_top">
        <div class="row">
          <h3 class="ttl_top">無料相談・お見積り</h3>
          <strong>まずは、お気軽にお電話、LINE、メールにてお問い合せください</strong>
          <p>どんなにちいさな仕事でも…　お庭周りのお悩み、疑問、いろいろ迷ってる…<br>など、お話だけでも是非お聞かせください。<br>丁寧にお答えします。</p>
          <p class="small">※繁忙期、現場調査＆外回りの為スタッフが 、事務所をあけることがございます<br>固定電話の留守番電話へメッセージを残していただけましたら、こちらから折り返しお電話させていただきます<br>その際は、 お名前・折り返しお電話可能な日にち・時間を、お知らせください。</p>
          <em>tel.<a class="tel" href="tel:0480-48-6177">0480-48-6177</a></em>
        </div>
      </div>
      <!--/.top-->
      <div class="gr_contact_center" id="line">
        <div class="row">
          <h3 class="ttl_gr"><span>LINEでのお問合せ</span></h3>
          <p>LINEアプリを使って簡単にお問い合わせができます。</p>
          <h4 class="hty">ご利用方法</h4>
          <p>QRコードまたは検索から友だち追加をしてください。</p>
          <ul class="list_l">
            <li><img src="<?php bloginfo('template_url')?>/contact/images/img_l_01.png?v=aa0ad5ba95c02b103f9ffcc25cd1ece4" alt=""></li>
            <li><img src="<?php bloginfo('template_url')?>/contact/images/img_l_02.png?v=2de25c58876770a6c3cf7370552fc5da" alt=""></li>
            <li><img src="<?php bloginfo('template_url')?>/contact/images/img_l_03.png?v=53f8cacb01392e8806153e78e0ea400f" alt=""></li>
          </ul>
          <div class="bx_qrcode">
            <div class="bx_qrcode_l">
              <em>ラインナップ・アカウント</em>
              <span>@zvw0249z</span>
              <em>を友だちに追加する</em>
            </div>
            <!--/.left-->
            <div class="bx_qrcode_ct">
              <span>or</span>
            </div>
            <!--/.center-->
            <div class="bx_qrcode_r">
              <figure>
                <figcaption>QRコードから<br>追加する</figcaption>
                <span><img src="<?php bloginfo('template_url')?>/contact/images/qrcode.png?v=c1c0eac61fc4222a7236ead8cb02c828" alt="QRCode"></span>
              </figure>
            </div>
            <!--/.right-->
          </div>
          <!--/.bx_qrcode-->
          <p>スマートフォンからホームページを見られている方は、「友だち追加」ボタンを押して簡単に追加できます。</p>
          <div class="btn_line"><a href="https://line.me/R/ti/p/%40zvw0249z" target="_blank"><img src="<?php bloginfo('template_url')?>/contact/images/bt_line.png" alt="btn line"></a></div>
          <p>友達追加が完了しましたら、お気軽にご相談内容のメッセージをお送り下さい。<br>もちろん、お写真などをお送りいただいても結構です。<br>担当者より確認出来次第ご返信致します。</p>
        </div>
      </div>
      <!--/.center-->
      <div class="gr_contact_bottom">
        <div class="row">
          <h3 class="ttl_gr"><span>無料相談・お見積り</span></h3>
          <p class="txt_top">必要事項をご記入の上送信してください。</p>
          <div class="ctn_form">
            <!-- <form class="frm_contact h-adr" action="" method="post"> -->
            <div class="frm_contact h-adr">
              <?php the_content();?>
            </div>
              <!--/.frm_btn-->
            <!-- </form> -->
          </div>
          <!--/.ctn_form-->
        </div>
      </div>
      <!--/.bottom-->
    </div>
    <!--/.gr_contact-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>