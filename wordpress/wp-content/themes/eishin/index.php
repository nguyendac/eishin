<?php get_header();?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <section id="fv" class="fv">
      <div class="fv_under">
        <picture>
          <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/banner_sp.jpg">
          <img src="<?php bloginfo('template_url')?>/images/banner.jpg?v=8bb9e0e5273547c2d0c796c4fae27c97" alt="banner">
        </picture>
      </div>
      <div class="fv_front">
        <figure>
          <img src="<?php bloginfo('template_url')?>/images/logo.png?v=59102aea6dd94d9fd194e03ec915529a" alt="logo" class="show_pc">
          <figcaption><img src="<?php bloginfo('template_url')?>/images/slogan.png?v=729809fd890c24c7504b7a9867837931" alt="笑顔と安心を囲む"></figcaption>
        </figure>
      </div>
    </section>
    <section class="st_concept">
      <div class="row">
        <h2 class="ttl_section">Concept</h2>
        <strong>「家族<ins>を</ins>つつむ<br><span>つよく やさしい 自然の庭」</span></strong>
        <p>ライフスタイルに合わせた住みごこちの良いお庭。<br>
        日々の暮らしの中にワクワクや喜びがたくさん感じられる。<br>
        そんなデザイン設計を目指しています。<br>
        人と人とのつながりを大切にし、一邸一邸まごころを込めて施工することを<br>
        モットーにお庭まわりをトータルプロデュースしております。</p>
        <div class="btn">
          <a href="/concept">more concept</a>
        </div>
        <!--/.btn-->
      </div>
    </section>
    <!--/.st_concept-->
    <section class="st_case">
      <div class="row wrap">
        <h2 class="ttl_section ttl_case">Case</h2>
        <div class="gr_case">
          <?php
          $cases = apply_filters('get_3_case','');
          while ($cases->have_posts()) : $cases->the_post();
          $terms = wp_get_post_terms($post->ID,'tag_case',array("fields" => "all"));
          ?>
          <article>
            <a href="<?php the_permalink()?>">
              <figure>
                <?php
                  $thumb = get_bloginfo('template_url').'/common/images/logo_sp.png';
                  if(get_post_meta($post->ID,'case_image',true)){
                    $thumb = get_post_meta($post->ID,'case_image',true)['url'];
                  }
                ?>
                <img src="<?php _e($thumb)?>" alt="<?php the_title()?>">
              </figure>
              <h3><?php the_title();?><br><?php _e(get_post_meta($post->ID,'case_location',true))?></h3>
            </a>
          </article>
          <?php endwhile;wp_reset_query();?>
        </div>
        <!--/.gr_case-->
      </div>
      <div class="row">
        <div class="btn btn_case">
          <a href="/case">more case</a>
        </div>
      </div>
    </section>
    <!--/.st_case-->
    <section class="st_news">
      <div class="row wrap">
        <h2 class="ttl_section ttl_new">News<span>新着情報</span></h2>
      </div>
      <div class="gr_news">
        <div class="row wrap">
          <div class="mark">
            <svg viewBox="0 0 202 197"  width="202" height="197">
              <defs>
                <use xlink:href="#thepath"/>
                <clipPath id="cloudclip">
                  <polygon fill="none" id="thepath" class="cls-1" points="56.05 197 0 126.69 20 39.02 101 0 182 39.02 202 126.69 145.95 197 56.05 197" />
                </clipPath>
              </defs>
            </svg>
          </div>
          <?php
            $articles = apply_filters('get_4_news','');
            while($articles->have_posts()):$articles->the_post();
            $terms = wp_get_post_terms($post->ID,'category',array("fields" => "all"));
          ?>
          <article>
            <a href="<?php the_permalink()?>">
              <?php
                $thumb = 'https://via.placeholder.com/202x196';
                if(has_post_thumbnail()) {
                  $thumb =  get_the_post_thumbnail_url($post->ID,array(202,196));
                }
              ?>
              <div class="wrap_svg">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202 197">
                  <image class="test" clip-path="url(#cloudclip)"  width="202" height="197" xlink:href="<?php _e($thumb)?>"></image>
                </svg>
              </div>
              <div class="main_art">
                <span><?php _e($terms[0]->name)?></span>
                <h3><?php the_title();?></h3>
                <p><?php the_excerpt()?></p>
              </div>
              <!--/.main_art-->
            </a>
          </article>
          <?php endwhile;wp_reset_query();?>
        </div>
      </div>
      <!--/.gr_news-->
      <div class="btn btn_new">
        <a href="/news">all view</a>
      </div>
    </section>
    <!--/.st_news-->
    <section class="st_about">
      <div class="row wrap">
        <div class="st_about_left">
          <h2 class="ttl_section ttl_about">About Us<span>わたしたちについて</span></h2>
          <p>埼玉県久喜市にある小さなエクステリア専門の会社です。</p>
          <div class="btn btn_about">
            <a href="/about">more company</a>
          </div>
        </div>
        <!--/.left-->
        <div class="st_about_right">
          <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3222.7556509346587!2d139.69656713293773!3d36.12381030580829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018b58f0952e8db%3A0x7994fd9362656d9a!2z5qCE6YCy44Ko44Kv44K544OG44Oq44KiIOS6i-WLmeaJgA!5e0!3m2!1sen!2s!4v1542207184831" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
        <!--/.right-->
      </div>
    </section>
    <!--/.st_about-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>