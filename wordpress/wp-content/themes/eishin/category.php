<?php
get_header();
?>
<body>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <div class="gr_ttl_left">
          <h2 class="ttl">News<span>新着情報</span></h2>
        </div>
        <!--/.left-->
        <div class="gr_ttl_right">
          <figure>
            <img src="<?php bloginfo('template_url')?>/news/images/img_ttl.jpg?v=d50581e70ff4103625eeb5d46adfc8ee" alt="Images title">
          </figure>
        </div>
        <!--/.right-->
      </div>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_breadcrumb show_pc">
      <div class="row">
        <ul class="gr_breadcrumb_list">
          <li><a href="/">ホーム</a></li>
          <li>新着情報</li>
        </ul>
        <!--/.list-->
      </div>
    </div>
    <!--/.gr_breadcrumb-->
    <section class="st_news">
      <div class="gr_tax">
        <div class="row">
          <ul class="list_tax">
            <li><a href="/news">ALL</a></li>
            <?php
              $types = apply_filters('list_taxo','category');
              foreach($types as $type) :
            ?>
            <li><a href="<?php _e(get_term_link($type))?>"><?php _e(nl2br($type->name)) ?></a></li>
          <?php endforeach;?>
          </ul>
          <!--/.list_tax-->
        </div>
      </div>
      <!--/.gr_tax-->
      <div class="row wrap">
        <h2 class="t_news">ALL</h2>
      </div>
      <div class="gr_news">
        <div class="row wrap">
          <div class="mark">
            <svg viewBox="0 0 202 197"  width="202" height="197">
              <defs>
                <use xlink:href="#thepath"/>
                <clipPath id="cloudclip">
                  <polygon fill="none" id="thepath" class="cls-1" points="56.05 197 0 126.69 20 39.02 101 0 182 39.02 202 126.69 145.95 197 56.05 197" />
                </clipPath>
              </defs>
            </svg>
          </div>
          <?php
            $term_name = single_cat_title('',false);
            $articles = apply_filters('list_article',$term_name,$_GET);
            while($articles->have_posts()):$articles->the_post();
          ?>
          <article>
            <a href="<?php the_permalink()?>">
              <?php
                $thumb = 'https://via.placeholder.com/202x196';
                if(has_post_thumbnail()) {
                  $thumb =  get_the_post_thumbnail_url($post->ID,array(202,196));
                }
              ?>
              <div class="wrap_svg">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202 197">
                  <image class="test" clip-path="url(#cloudclip)"  width="202" height="197" xlink:href="<?php _e($thumb)?>"></image>
                </svg>
              </div>
              <div class="main_art">
                <span><?php single_cat_title()?></span>
                <h3><?php the_title();?></h3>
                <p><?php the_excerpt()?></p>
              </div>
              <!--/.main_art-->
            </a>
          </article>
          <?php endwhile;wp_reset_query();?>
        </div>
      </div>
      <!--/.gr_news-->
      <div class="pag_news">
        <div class="row">
          <?php
          mp_pagination($prev = '', $next = '', $pages=$articles->max_num_pages);
          wp_reset_query();
          ?>
        </div>
      </div>
    </section>
    <!--/.st_news-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>