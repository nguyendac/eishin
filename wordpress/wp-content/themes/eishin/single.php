<?php get_header()?>
<body>
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <div class="gr_ttl_left">
          <h2 class="ttl">News<span>新着情報</span></h2>
        </div>
        <!--/.left-->
        <div class="gr_ttl_right">
          <figure>
            <img src="<?php bloginfo('template_url')?>/news/images/img_ttl.jpg?v=d50581e70ff4103625eeb5d46adfc8ee" alt="Images title">
          </figure>
        </div>
        <!--/.right-->
      </div>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_breadcrumb show_pc">
      <div class="row">
        <ul class="gr_breadcrumb_list">
          <li><a href="/">ホーム</a></li>
          <li><a href="/news">新着情報</a></li>
          <li><?php the_title()?></li>
        </ul>
        <!--/.list-->
      </div>
    </div>
    <!--/.gr_breadcrumb-->
    <div class="ctn_blog">
      <div class="row wrap">
        <div class="bx_new">
          <div class="top_tt">
            <?php $terms = wp_get_post_terms($post->ID,'category',array("fields" => "all"))[0];?>
            <span><?php _e($terms->name)?></span>
            <h3><?php the_title()?></h3>
          </div>
          <!--/.top_tt-->
          <?php
          if(has_post_thumbnail()):?>
          <figure>
            <img src="<?php echo get_the_post_thumbnail_url($post->ID,'full')?>" alt="Images">
          </figure>
          <?php endif;?>
          <?php _e(nl2br(the_content()))?>
          <div class="social">
            <?php echo do_shortcode('[Sassy_Social_Share type="floating"]')?>
          </div>
          <!--/.social-->
          <div class="b_pag">
            <ul class="list_pag">
              <li class="prev">
                <?php previous_post_link('%link','<span>前へ</span>',false); ?>
              </li>
              <li><a href="<?php _e(home_url())?>/news">一覧へ戻る</a></li>
              <li><?php next_post_link('%link','<span>次へ</span>',false); ?></li>
            </ul>
            <!--/.list_pag-->
          </div>
          <!--/.b_pag-->
        </div>
        <!--/.bx_new-->
        <?php get_template_part('news/news','sidebar')?>
      </div>
    </div>
    <!--/.ctn_blog-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<?php endwhile; endif; ?>
</body>
</html>