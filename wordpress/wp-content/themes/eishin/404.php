<?php
get_header();
?>
<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <div class="gr_ttl_left">
          <h2 class="ttl">Page Not Found</h2>
        </div>
        <!--/.left-->
        <div class="gr_ttl_right">
          <figure>
            <img src="<?php bloginfo('template_url')?>/concept/images/img_ttl.jpg?v=d8d591e4b1b1033a1d126f891dce9634" alt="Images title">
          </figure>
        </div>
        <!--/.right-->
      </div>
    </div>
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>