<?php
/*
Template Name: Case Template Page
*/
get_header();
?>
<body>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <div class="gr_ttl_left">
          <h2 class="ttl">Case<span>施工実績</span></h2>
        </div>
        <!--/.left-->
        <div class="gr_ttl_right">
          <figure>
            <img src="<?php bloginfo('template_url')?>/case/images/img_ttl.jpg?v=dc170dc9d3d2952dead6c12bca6224af" alt="Images title">
          </figure>
        </div>
        <!--/.right-->
      </div>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_breadcrumb">
      <div class="row">
        <ul class="gr_breadcrumb_list">
          <li><a href="<?php _e(home_url())?>/">ホーム</a></li>
          <li>施工実績</li>
        </ul>
        <!--/.list-->
      </div>
    </div>
    <!--/.gr_breadcrumb-->
    <div class="gr_case">
      <h3 class="ttl_gr"><span>施工内容から探す</span></h3>
      <div class="case_tag">
        <div class="row">
          <ul class="list_tag">
            <?php
            $types = apply_filters('list_taxo','tag_case');
            if($types):
            foreach($types as $type) :
            ?>
            <li><a href="<?php _e(get_term_link($type))?>"><?php _e(nl2br($type->name)) ?></a></li>
            <?php endforeach;endif;?>
          </ul>
          <!--/.list_tag-->
        </div>
      </div>
      <!--/.b_tag-->
      <div class="case_item b_bkg">
        <div class="row wrap">
          <div class="w_item">
            <?php
            $cases = apply_filters('list_case','');
            while ($cases->have_posts()) : $cases->the_post();
            $terms = wp_get_post_terms($post->ID,'tag_case',array("fields" => "all"));
            ?>
            <article>
              <a href="<?php the_permalink()?>">
                <figure>
                  <?php
                    $thumb = get_bloginfo('template_url').'/common/images/logo_sp.png';
                    if(get_post_meta($post->ID,'case_image',true)){
                      $thumb = get_post_meta($post->ID,'case_image',true)['url'];
                    }
                  ?>
                  <img src="<?php _e($thumb)?>" alt="Images Case 1">
                </figure>
                <div class="main_art">
                  <h4><?php the_title();?><span><?php _e(get_post_meta($post->ID,'case_location',true))?></span></h4>
                  <ul class="main_art_l">
                    <?php foreach($terms as $term):?>
                    <li><span><?php _e($term->name)?></span></li>
                    <?php endforeach;?>
                  </ul>
                  <!--/.list-->
                </div>
                <!--/.main_art-->
              </a>
            </article>
          <?php endwhile;wp_reset_query();?>
          </div>
        </div>
      </div>
    </div>
    <!--/.gr_case-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>