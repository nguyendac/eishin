<div class="sidebar">
  <dl>
    <dt>CATEGORY</dt>
    <dd>
      <ul class="list_wg">
        <?php
          $types = apply_filters('list_taxo','category');
          foreach($types as $type) :
        ?>
        <li><a href="<?php _e(get_term_link($type))?>"><?php _e(nl2br($type->name)) ?></a></li>
      <?php endforeach;?>
      </ul>
    </dd>
  </dl>
  <dl>
    <dt>ARCHIVE</dt>
    <dd>
      <form action="/news" method="get" id="search">
        <div class="bx_slt" id="sandbox-container">
          <input class="slt_time" type="text" id="flow_date" name="date" placeholder="日付を選ぶ">
        </div>
        <!--/.bx_slt-->
      </form>
    </dd>
  </dl>
</div>