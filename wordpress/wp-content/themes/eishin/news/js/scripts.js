$(document).ready(function() {
  $('#sandbox-container input').datepicker({
    todayHighlight: true,
    autoclose: true,
    language: 'ja',
  });
  $('#sandbox-container input').on('keydown',function(e){
    e.preventDefault();
    return;
  })
  $('#flow_date').change(function(){
    if($(this).val()){
      $('#search').submit();
    }
  })
});