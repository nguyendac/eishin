<div class="header_main">
  <h1>
    <a href="<?php _e(home_url())?>/">
      <picture>
        <source media="(max-width:768px)" srcset="<?php bloginfo('template_url')?>/common/images/logo_sp.png">
        <img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="logo">
      </picture>
    </a>
  </h1>
  <div class="wrap_nav">
    <nav class="header_nav row" id="nav">
      <ul class="header_nav_list">
        <li><a href="<?php _e(home_url())?>/">HOME</a></li>
        <li>
          <a href="<?php _e(home_url())?>/concept">コンセプト</a>
          <span class="sub show_sp"></span>
          <ul>
            <li><a href="<?php _e(home_url())?>/concept#flow">施工までの流れ</a></li>
          </ul>
        </li>
        <li class="has_child">
          <a href="<?php _e(home_url())?>/case">施工実績</a>
        </li>
        <li class="has_child">
          <a href="<?php _e(home_url())?>/news">新着情報</a>
        </li>
        <li class="has_child">
          <a href="<?php _e(home_url())?>/about">会社案内</a>
          <span class="sub"></span>
          <ul>
            <li><a href="<?php _e(home_url())?>/about#message">代表あいさつ</a></li>
            <li><a href="<?php _e(home_url())?>/about#company">会社概要</a></li>
            <li><a href="<?php _e(home_url())?>/about#access">アクセス</a></li>
          </ul>
        </li>
        <li class="show_sp"><a href="<?php _e(home_url())?>/contact">お問い合わせ</a></li>
      </ul>
      <ul class="header_nav_btn">
        <li><a href="<?php _e(home_url())?>/contact" class="mail">お問い合わせ</a></li>
        <li><a href="tel:0480-48-6177" class="tel">0480-48-6177</a></li>
      </ul>
    </nav>
  </div>
  <div class="menu_sp show_sp" id="icon_nav">
      <div class="icon_menu">
        <div class="icon_inner"></div>
      </div>
    </div>
</div>