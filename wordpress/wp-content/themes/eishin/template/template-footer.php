<div class="footer_top">
  <div class="footer_top_nav">
    <ul class="row">
      <li><a href="<?php _e(home_url())?>/">HOME</a></li>
      <li>
        <a href="<?php _e(home_url())?>/concept">コンセプト</a>
        <span class="sub show_sp"></span>
        <ul>
          <li><a href="<?php _e(home_url())?>/concept#flow">施工までの流れ</a></li>
        </ul>
      </li>
      <li class="has_child">
        <a href="<?php _e(home_url())?>/news">新着情報</a>
      </li>
      <li class="has_child">
        <a href="<?php _e(home_url())?>/case">施工実績</a>
      </li>
      <li class="has_child">
        <a href="<?php _e(home_url())?>/about">会社案内</a>
        <span class="sub show_sp"></span>
        <ul>
          <li><a href="<?php _e(home_url())?>/about#message">代表あいさつ</a></li>
          <li><a href="<?php _e(home_url())?>/about#company">会社概要</a></li>
          <li><a href="<?php _e(home_url())?>/about#access">アクセス</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="footer_top_c">
    <div class="footer_top_c_row row">
      <article>
        <h3>Contact <span>お問い合わせ</span></h3>
        <p class="hotline">
          <a href="tel:0480-48-6177">0480-48-6177</a>
          <span>営業時間 9：30～18：00</span>
        </p>
        <div class="btn"><a href="/contact">お問い合わせフォーム</a></div>
        <div class="show_pc">
          <h4>栄進エクステリア</h4>
          <address>〒349-1123 埼玉県久喜市間鎌265</address>
          <p>TEL:<a href="tel:0480-48-6177" class="tel">0480-48-6177</a><span> FAX:0480-48-6177</span></p>
        </div>
      </article>
    </div>
  </div>
  <div class="footer_top_e show_sp">
    <h4>栄進エクステリア</h4>
    <div class="footer_top_e_r">
      <address>〒349-1123 埼玉県久喜市間鎌265</address>
      <p>TEL:<a href="tel:0480-48-6177" class="tel">0480-48-6177</a><span> FAX:0480-48-6177</span></p>
    </div>
  </div>
</div>
<div class="footer_bottom">Copyright &copy; eishin All Rights Reserved.</div>
<div class="pagetop" id="pagetop">Top</div>
<ul class="social show_pc">
  <li><a href="/contact"><img src="<?php bloginfo('template_url')?>/common/images/icon_contact.png" alt=""></a></li>
  <li><a href="/contact/#line" target="_blank"><img src="<?php bloginfo('template_url')?>/common/images/icon_line.png" alt=""></a></li>
  <li><a href="https://www.instagram.com/eishin2010/?hl=ja" target="_blank"><img src="<?php bloginfo('template_url')?>/common/images/icon_ins.png" alt=""></a></li>
  <li><a href="https://twitter.com/yhngxtpocynd988" target="_blank"><img src="<?php bloginfo('template_url')?>/common/images/icon_twitter.png" alt=""></a></li>
  <li><a href="https://www.facebook.com/Eishin-Exterior-2095588943818353/" target="_blank"><img src="<?php bloginfo('template_url')?>/common/images/icon_face.png" alt=""></a></li>
</ul>