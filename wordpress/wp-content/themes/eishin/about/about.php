<?php
/*
Template Name: About Page
*/
get_header();
?>

<body>

<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('template/template','header')?>
  </header>
  <main>
    <div class="gr_ttl">
      <div class="row wrap">
        <div class="gr_ttl_left">
          <h2 class="ttl">About Us<span>会社案内</span></h2>
        </div>
        <!--/.left-->
        <div class="gr_ttl_right">
          <figure>
            <img src="<?php bloginfo('template_url')?>/about/images/img_ttl.jpg?v=69d190f327918b832349f6f12e4e770a" alt="Images title">
          </figure>
        </div>
        <!--/.right-->
      </div>
    </div>
    <!--/.gr_ttl-->
    <div class="gr_breadcrumb show_pc">
      <div class="row">
        <ul class="gr_breadcrumb_list">
          <li><a href="<?php _e(home_url())?>">ホーム</a></li>
          <li>会社案内</li>
        </ul>
        <!--/.list-->
      </div>
    </div>
    <!--/.gr_breadcrumb-->
    <div class="gr_about">
      <h3 class="ttl_gr"><span>MESSAGE</span></h3>
      <div class="gr_about_top">
        <div class="row">
          <div class="b_avt">
            <figure>
              <img src="<?php bloginfo('template_url')?>/about/images/img_large.jpg?v=6c1c54414250c84180a2ed70949cf49b" alt="Avatar">
            </figure>
            <div class="right">
              <p>栄進エクステリア代表白石篤志です。</p>
              <p>妻の父が左官工事と外構工事の仕事をしており、高校生の頃にバイトを
              させてもらった事がこの職に就いたキッカケです。<br class="show_pc">
              高校卒業後、職人としてエクステリア、外構工事専門の会社に就職し育
              てて戴き、エクステリア、外構工事一筋に修行を致しました。</p>
              <p>25歳の時に独立し、小さなマンションの一部屋を事務所にし、外構工事
              の専門の会社を開業しました。2014年からは自宅兼事務所を久喜市間鎌
              に建設し、そちらを拠点に仕事に取り組んでいます。<br class="show_pc">
              自然が生活のすぐ隣に寄り添っているような外構を作りたい。<br class="show_pc">
              自然が生み出すものはみな美しいと思いますが、特に日本の気候から成る
              四季折々の表情を見せてくれる樹木の美しさというのは本当に素晴らしい
              ですね。外構工事を通じて街に多くの自然を作り、住宅と調和したお庭を
              作っていきたいと思います。<br class="show_pc">
              まだ勉強中の身分ではございますが、デザインも、施工も一生懸命に頑張
              ります。宜しくお願い致します。</p>
            </div>
            <!--/.right-->
          </div>
          <!--/.b_avt-->
          <div class="b_dl">
            <div class="b_dl_left">
              <dl>
                <dt>出身：</dt>
                <dd>越谷市</dd>
              </dl>
              <dl>
                <dt>血液型：</dt>
                <dd>O型</dd>
              </dl>
              <dl>
                <dt>家族：</dt>
                <dd>４人(愛娘＆息子)＆トイプードル</dd>
              </dl>
            </div>
            <!--/.left-->
            <div class="b_dl_right">
              <dl>
                <dt>好きなｺト：</dt>
                <dd>時間ができると三線を引く(石垣島でオーダーメイド)<br>親族３家族で行く旅行。ウエイトトレーニング。</dd>
              </dl>
              <dl>
                <dt>好きなモノ：</dt>
                <dd>本、銘酒(日本酒、ウイスキー)</dd>
              </dl>
            </div>
            <!--/.right-->
          </div>
          <!--/.b_dl-->
        </div>
        <div class="gr_img">
          <div class="row">
            <ul class="list_img">
              <li><img src="<?php bloginfo('template_url')?>/about/images/img_s_01.png?v=dc4c2cc585427684afb6d7e362fec0e4" alt="Images 01"></li>
              <li><img src="<?php bloginfo('template_url')?>/about/images/img_s_02.png?v=e4023d891d92d4fb5531a5465b8d48a2" alt="Images 02"></li>
              <li><img src="<?php bloginfo('template_url')?>/about/images/img_s_03.png?v=6e5689c122f148af56067fad97ed0311" alt="Images 03"></li>
              <li><img src="<?php bloginfo('template_url')?>/about/images/img_s_04.png?v=1940543dfe106a375909187bfdabfcda" alt="Images 04"></li>
              <li><img src="<?php bloginfo('template_url')?>/about/images/img_s_05.png?v=e2d30c798ec8f79587e615be50276d89" alt="Images 05"></li>
            </ul>
            <!--/.list_img-->
          </div>
        </div>
        <!--/.gr_img-->
      </div>
      <!--/.top-->
      <div class="gr_about_center">
        <div class="row">
          <h3 class="ttl_gr"><span>Company Profile</span></h3>
          <div class="b_profile">
            <dl>
              <dt>会社名</dt>
              <dd>
                <em>栄進エクステリア</em>
              </dd>
            </dl>
            <dl>
              <dt>代　表</dt>
              <dd>
                <em>白石  篤志</em>
              </dd>
            </dl>
            <dl>
              <dt>設　立</dt>
              <dd>
                <em>2010年4月</em>
              </dd>
            </dl>
            <dl>
              <dt>住　所</dt>
              <dd>
                <em>〒349-1123　埼玉県久喜市間鎌265-46</em>
              </dd>
            </dl>
            <dl>
              <dt>電話番号</dt>
              <dd>
                <em><a class="tel" href="tel:048-048-6177">048-048-6177</a></em>
              </dd>
            </dl>
            <dl>
              <dt>工事内容</dt>
              <dd>
                <em>車庫まわり</em>
                <ul>
                  <li>・土間コンクリート・カーポート・サイクルポート</li>
                  <li>・カーゲート・電動シャッター</li>
                </ul>
                <em>門まわり</em>
                <ul>
                  <li>・ブロック塀(左官仕上げなど)・門扉・表札、ポスト</li>
                  <li>・アプローチ(タイル、石貼り、インターロッキング、真砂土)</li>
                </ul>
                <em>お庭まわり</em>
                <ul>
                  <li>・テラス(ウッドデッキ、タイル、石貼り)</li>
                  <li>・ガーデンルーム・パーゴラ・水栓・物置・坪庭</li>
                  <li>・植栽、剪定管理</li>
                </ul>
              </dd>
            </dl>
            <dl>
              <dt>登　録</dt>
              <dd>
                <ul>
                  <li>・コンクリートブロック工事士　第4067号</li>
                  <li>・庭園管理士　第105337767号</li>
                  <li>・ライティングマイスター　第010137号</li>
                  <li>・ウォーターガーデンマイスター　第030069号</li>
                  <li>・LIXILエクステリアマイスター</li>
                  <li>・LIXILリフォームネット会員</li>
                </ul>
              </dd>
            </dl>
          </div>
        </div>
      </div>
      <!--/.center-->
      <div class="gr_about_access">
        <h3 class="ttl_gr"><span>Access</span></h3>
        <div class="map">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1611.3416572333065!2d139.6964195032502!3d36.12557232331358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018b5858cfb7a4d%3A0x824e4b1115c6fbe9!2s265+Magama%2C+Kuki-shi%2C+Saitama-ken+349-1123%2C+Japan!5e0!3m2!1sen!2s!4v1542300805500" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!--/.map-->
      </div>
      <!--/.access-->
      <div class="gr_about_bottom">
        <h3 class="ttl_gr"><span>対応エリア</span></h3>
        <div class="row">
          <figure>
            <img src="<?php bloginfo('template_url')?>/about/images/map.png?v=5db4a5ac406f68dd7a77cc2f3635c116" alt="">
          </figure>
          <div class="right">
            <p>久喜市、幸手市、杉戸町、鷲宮町、加須市、<br>
            宮代町、春日部市、越谷市、松伏町、吉川市、<br>
            羽生市、桶川市、鴻巣市、北本市、伊奈町、<br>
            上尾市、蓮田市、白岡町、騎西町、菖蒲町、</p>
            <p>を中心に業務を行っております。<br>宜しく御願致します。</p>
          </div>
        </div>
      </div>
      <!--/.bottom-->
    </div>
    <!--/.gr_about-->
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('template/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>