<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eishin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eVP&hwLwW_Ir&FwiBtvclCMZ(R&))WUsCey/+wQ8hR*m)n75d^Jnhnv:q]_eW{9j');
define('SECURE_AUTH_KEY',  '[y8HjXKdmpRWUn8XupWJ]n@~nmdj.LJp D(kENszxeZV4S^LP!p2K^[}6H,Sio|C');
define('LOGGED_IN_KEY',    'jAgN)Eqh&Gf8<y1*+V+6I{Z(=T~Ur(*2t:4g!Jbx-ON>% z!}0aa%N29xVM2`qVv');
define('NONCE_KEY',        '1FFc#>@+|S|gGm nZ44miK|YSO%0~q~4iPJ8_F~NAmyU7fVC9e^~BH|-:$=TkOa7');
define('AUTH_SALT',        '$(i3p6$iu:KF/<F-,}bzA%jU2y!bY4C6LUkQ5j7>q~%UjryQPtHuqHE-I!d0NWA8');
define('SECURE_AUTH_SALT', 'f6:]E: m5.NZ3S1fX`%_qxb+%z$%a@{w`3zz/~tYX6rJDTd`5s>L9Z]t8qB*NBA0');
define('LOGGED_IN_SALT',   'wMt5k48~UPXI:K-^x6NIH|$.5*Z%U^%MU(!.c];[qVq#7$au7B^$`u9.-y^J#P&A');
define('NONCE_SALT',       'k@+C)Y]z&w)w7gI0[+YGR~P>necOszTM7>A,Jz|mlCv)P;dY7|B<c)?{H.e(%)n<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
